(function () {
  var button = document.getElementById('scroll-btn');
  var to;

  button.addEventListener('click', smoothScroll);
  window.addEventListener('scroll', scrollListener);
  scrollListener();

  function scrollListener() {
    clearTimeout(to);
    if (document.body.scrollTop > 200) {
      button.style.display = 'block';
      to = setTimeout(function () {
        button.style.opacity = '1';
      }, 0);
    } else {
      button.style.opacity = '0';
      to = setTimeout(function () {
        button.style.display = 'none';
      }, 500);
    }
  }

  function smoothScroll() {
    var duration = 500;
    var startLocation = window.pageYOffset;
    var endLocation = 0;
    var distance = endLocation - startLocation;
    var increments = distance/(duration/16);

    var runAnimation = setInterval(animateScroll, 16);

    function animateScroll() {
      window.scrollBy(0, increments);
      checkTopReached();
    };

    function checkTopReached() {
      var travelled = window.pageYOffset;
      if ( travelled <= (endLocation || 0) ) {
        clearInterval(runAnimation);
      }
    }
  }
})();
